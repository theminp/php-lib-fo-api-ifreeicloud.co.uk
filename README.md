# README #

This lib help you used iFree API

### How do I get set up? ###

* Add ifree.php to you lib folders
* Add you iFree PHP API-Key in lib instead of << You_token_here >>
* Scroll down and search demo usage:)
* IMEI order result & service list have json format but balance as text.

### Demo usage >> ###
    //init:
    require "ifree.php";
    
    $imei = '123456789098765';
    $service = '0'; //Service '0' = Get model by imei or sn

    //Get balance: 
    $balance = $ifree->get_balance();
    echo 'You account balance is ' . $balance . '$';


    //Get service list:
    $list = $ifree->get_list();
    echo "<hr><pre>".print_r($list->object, true)."</pre><hr>";

    //order by imei:
    $result = $ifree->imei_order($imei, $service);
    echo "<hr><pre>".print_r($result->object, true)."</pre><hr>";
