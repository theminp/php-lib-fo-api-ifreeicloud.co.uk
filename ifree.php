<?php

class ifree
{
    private $api_token;
    private $url;
    private $service;
    private $imei;

    public function __construct()
    {
        $this->url = "https://api.ifreeicloud.co.uk";
        $this->api_token = "<< You_token_here >>";
    }

    public function get_list()
    {
        $myCheck["accountinfo"] = "servicelist";
        $myCheck["key"] = $this->api_token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $myCheck);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $result = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result);
        return ($json);
    }

    public function get_balance()
    {
        $myCheck["accountinfo"] = "balance";
        $myCheck["key"] = $this->api_token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $myCheck);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $result = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result);
        return ($json->object->account_balance);
    }

    public function imei_order($imei , $service)
    {
        $myCheck["service"] = $service;
        $myCheck["imei"] = $imei;
        $myCheck["key"] = $this->api_token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $myCheck);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $result = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result);
        return ($json);
    }
}